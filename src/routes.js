/* eslint-disable import/no-named-as-default */

import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App.js';
import PageNotFound from './components/PageNotFound.js';

export default (
  <Route path="/" component={App} name="Home">
    <IndexRoute component={App} />
    <Route path="*" component={PageNotFound} name="Not Found" />
  </Route>
);
