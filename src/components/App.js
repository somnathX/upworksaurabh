import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Layout } from 'react-toolbox';
//import Main from './Main';
import LoginPage from './Auth/LoginPage';
import { Panel } from 'react-toolbox';
import MainContent from './MainContent';

const App = (props) => (
  <Layout>
      <LoginPage />
  </Layout>
);

export default App;
