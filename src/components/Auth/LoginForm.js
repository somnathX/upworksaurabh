import React from 'react';
import { Card, CardActions } from 'react-toolbox/lib/card';
import style from './style.scss'; // eslint-disable-line
import { Container, Segment, Grid, Input, Form, Checkbox, Button } from 'semantic-ui-react';
import { Panel } from 'react-toolbox';
const LoginForm = ({creds, login, onChange}) => {

  return (
      <div>
        <Form onSubmit={login}>
          <div className={style.group}>
            <Card className={style.dashboardCard}>

              <Form.Field>
                <label className={style.customLabel}>Email or username</label>
                <input placeholder='Email or username' />
              </Form.Field>

              <Form.Field>
                <label className={style.customLabel}>Password</label>
                <input placeholder='Password' />
              </Form.Field>

              <div className={style.customLinkDiv}>
                <a className={style.primaryColor}>Forgot password?</a>
              </div>

              <Form.Field
                  control={Checkbox}
                  className={style.customCheckBoxLabel}
                  label={{ children: 'Remember me' }}
              />

              <Form.Field className={style.buttonDiv}>
                <Button className={style.button}>LOGIN</Button>
              </Form.Field>
            </Card>
          </div>
        </Form>
        </div>
  );
};

LoginForm.propTypes = {
  creds: React.PropTypes.object.isRequired,
  login: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired
};

export default LoginForm;
