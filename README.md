
## Quick Start

Clone project and install dependencies:
$ npm install
```

Start the app:
```bash
$ npm start -s
```

Build the app:
```bash
$ npm run build
```